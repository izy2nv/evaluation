import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';

export class ModalComponent extends Component {
hideModal() {
  this.props.onHide()
}
    render() {
        return (
            <Modal
            {...this.props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Add Information
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {this.props.content}
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.props.onHide}>Close</Button>
            </Modal.Footer>
          </Modal>
        );
    }
}