import React, { Component } from 'react';
import SuppliersForm from './suppliersForm';
import companySuppliers from '../models/suppliers.json';
import { ModalComponent } from './modal';
import { Button, ButtonToolbar } from 'react-bootstrap';
import { connect } from 'react-redux';
import * as actions from '../actions/suppliersActions';
import { bindActionCreators } from "redux";

class SuppliersList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentIndex: -1,
            listOfSuppliers: this.returnSuppliers(),
            category: '',
            showModal: false
        }
    }
    title = '';

    tableHeaders = ['Name', 'Email', 'Phone', 'Address', 'Action'];
    returnSuppliers() {
        return JSON.parse(localStorage.getItem('suppliers'));
    }

    editSupplierInfo = index => {
        console.log(index);
        this.setState({ showModal: true });
        this.props.updateTransactionIndex(index);
        // this.setState({ showModal: true });
    }

    setDefaultTitle = () => {
        this.title = 'Add Information';
    }

    filterByCategory = e => {
        this.setState({ category: e.target.value });
        let data = this.returnSuppliers().filter(function (result) {
            return result.category === e.target.value;
        });
        this.setState({listOfSuppliers: data});
    };

    getUnique(arr, comp) {
        const unique = arr
            .map(e => e[comp])
            .map((e, i, final) => final.indexOf(e) === i && i)
            .filter(e => arr[e])
            .map(e => arr[e]);
        return unique;
    }

    render() {
        const suppliers = this.getUnique(companySuppliers.suppliers, "category");
        let closeModal = () => this.setState({ showModal: false });
        return (
            <div className="container card mt-3">
                <h4 className="text-center mt-2">Suppliers List</h4>
                <form className="mt-3">
                {/* <label>Category</label> */}
                    <select className="form-control col-md-3 mb-2" value={this.state.category} onChange={this.filterByCategory}>
                        {suppliers.map(supplier => (
                            <option key={supplier.id} value={supplier.category}>
                                {supplier.category}
                            </option>
                        ))}
                    </select>
                </form>
                <table className="table table-striped table-light">
                    <thead>
                        <tr>
                            {
                                this.tableHeaders.map((item, index) => {
                                    return <th scope="col" key={index}>{item}</th>
                                })
                            }
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.listOfSuppliers.map((item, index) => {
                                return <tr key={index}>
                                    <td>{item.supplierName}</td>
                                    <td>{item.supplierEmail}</td>
                                    <td>{item.supplierPhoneNo}</td>
                                    <td>
                                        <div>{item.line1}, {item.line2}</div>
                                        <div>{item.city}, {item.state}, {item.zipCode}</div>
                                    </td>
                                    <td className="text-center">
                                        <button type="button" className="btn btn-link" title="Update Information" onClick={() => this.editSupplierInfo(index)}><i className="fa fa-edit editIcon"></i></button>
                                    </td>
                                </tr>
                            })
                        }
                    </tbody>
                </table>
                <ButtonToolbar className="mb-3">
                    <Button variant='primary' onClick={() => this.setState({ showModal: true })}>Add Supplier</Button>
                </ButtonToolbar>
                <ModalComponent show={this.state.showModal} onHide={closeModal} content={<SuppliersForm onCrudSuccess={closeModal} />} />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        listOfSuppliers: state.listOfSuppliers
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        updateTransactionIndex: actions.updateIndex
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps) (SuppliersList);

