import React, { Component } from 'react';
import states from '../models/states.json';
import { connect } from 'react-redux';
import * as actions from '../actions/suppliersActions';
import { bindActionCreators } from "redux";

class SuppliersForm extends Component {
    listOfstates = states.states;
    state = {
        ...this.returnStateObject()
    }

    title = '';

    returnStateObject() {
        if (this.props.currentIndex === -1) {
            return {
                supplierName: '',
                supplierEmail: '',
                supplierPhoneNo: '',
                line1: '',
                line2: '',
                city: '',
                state: '',
                zipCode: '',
                category: ''
            }
        } else
            return this.props.listOfSuppliers[this.props.currentIndex]
    }

    componentDidUpdate(prevProps) {
        if (prevProps.currentIndex !== this.props.currentIndex || prevProps.listOfSuppliers.length !== this.props.listOfSuppliers.length)
            this.setState({ ...this.returnStateObject() })
    }

    handleInputChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    addSupplier = e => {
        e.preventDefault();
        if (this.props.currentIndex === -1)
            this.props.insertTransaction(this.state)
        else
            this.props.updateTransaction(this.state)
        this.props.onCrudSuccess();
    }

    render() {
        for (var i = 0; i < this.listOfstates.length; i++) {
            this.listOfstates[i].id = i;
        }
        return (
            <form onSubmit={this.addSupplier} autoComplete="off">
                <div className="form-group col-md-7 row">
                    <input className="form-control" name="category" placeholder="Category" title="Enter Category" value={this.state.category} onChange={this.handleInputChange} />
                </div>
                <div className="form-group col-md-7 row">
                    <input className="form-control" name="supplierName" placeholder="Company Name" title="Supplier Name" value={this.state.supplierName} onChange={this.handleInputChange} />
                </div>
                <div className="form-group col-md-6 row">
                    <input className="form-control" name="supplierEmail" placeholder="Email" title="Supplier Email" value={this.state.supplierEmail} onChange={this.handleInputChange} />
                </div>
                <div className="form-group col-md-4 row">
                    <input className="form-control" name="supplierPhoneNo" placeholder="Phone Number" value={this.state.supplierPhoneNo} onChange={this.handleInputChange} />
                </div>
                <div className="form-group">
                    <input className="form-group form-control col-md-7" name="line1" placeholder="Street Address 1" value={this.state.line1} onChange={this.handleInputChange} />
                    <input className="form-group form-control col-md-5" name="line2" placeholder="Street Address 2" value={this.state.line2} onChange={this.handleInputChange} />
                </div>
                <div className="form-group row col">
                    <input className="form-group form-control col-md-3" name="city" placeholder="City" value={this.state.city} onChange={this.handleInputChange} />
                    <select className="form-control col-md-2 ml-3" name="state" value={this.state.state} onChange={this.handleInputChange}>
                        {this.listOfstates.map(state => (
                            <option key={state.id} value={state.abbreviation}>
                                {state.abbreviation}
                            </option>
                        ))}
                    </select>
                    <input className="form-group form-control col-md-3 ml-3" name="zipCode" placeholder="Zip Code" value={this.state.zipCode} onChange={this.handleInputChange} />
                </div>
                <button className="btn btn-success" type="submit">Submit</button>
            </form>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        listOfSuppliers: state.listOfSuppliers,
        currentIndex: state.currentIndex
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        insertTransaction: actions.insert,
        updateTransaction: actions.update
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(SuppliersForm);