import React from 'react';
import logo from './logo.svg';
import './App.css';
import SuppliersList from './components/suppliersList';

function App() {
  return (
   <SuppliersList />
  );
}

export default App;
