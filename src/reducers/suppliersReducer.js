export const suppliersReducer = (state, action) => {
    var listOfSuppliers = JSON.parse(localStorage.getItem('suppliers'));
    function updateDB(data) {
        localStorage.setItem('suppliers', JSON.stringify(data));
    }
    switch(action.type) {
        case 'INSERT':
        listOfSuppliers.push(action.payload);
        updateDB(listOfSuppliers);
        return { listOfSuppliers, currentIndex: -1 };

        case 'UPDATE':
        listOfSuppliers[state.currentIndex] = action.payload;
        updateDB(listOfSuppliers);
        return { listOfSuppliers, currentIndex: -1 };

        case 'UPDATE_INDEX':
        return { listOfSuppliers, currentIndex: action.payload };

        default:
        return state;
    }
}